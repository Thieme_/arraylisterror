package com.pikathieme.main;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class BuildMode implements CommandExecutor, Listener {

	public ArrayList<Player> array = new ArrayList<Player>();

	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
		if (command.getName().equalsIgnoreCase("build")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				if (p.hasPermission("lobby.edit")) {
					if (array.contains(p)) {
						p.getInventory().clear();
						array.remove(p);
						p.sendMessage("§3>> §7Buildmode staat nu uit!");
					} else {
						array.add(p);
						p.sendMessage("§3>> §7Buildmode staat nu aan!");
					}
				}
			} else {
				sender.sendMessage("Players only");
			}
		}
		return true;
	}

	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action a = e.getAction();
		if (e.getHand() == EquipmentSlot.HAND) {
			if (p.getEquipment().getItemInMainHand() != null
					&& p.getEquipment().getItemInMainHand().getItemMeta() != null) {
				if (p.getEquipment().getItemInMainHand().getItemMeta().hasDisplayName()) {
					if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
						e.setCancelled(true);
						if (p.getEquipment().getItemInMainHand().getType() == Material.ENDER_CHEST) {
							deGUI(p);
						}
						if (p.getEquipment().getItemInMainHand().getType() == Material.COMPASS) {

						}
					}
				}
			}
		}

	}

	public void deGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 54, "§7§kiii§6§lToggle Menu§7§kiii");

		player.openInventory(inv);
		player.updateInventory();
	}
}
